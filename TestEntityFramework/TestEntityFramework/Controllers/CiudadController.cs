﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;

namespace TestEntityFramework.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CiudadController : ControllerBase
    {
        private readonly DBContext _context;
        public CiudadController(DBContext context)
        {
            _context = context;
        }
        // GET: api/Ciudad
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_context.Ciudad.AsQueryable());
        }

        // GET: api/Ciudad/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(long id)
        {
            var result = _context.Ciudad.Find(id);
            return Ok(result);
        }

        // POST: api/Ciudad
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] Ciudad value)
        {
            var result = await _context.Ciudad.AddAsync(value);
            await _context.SaveChangesAsync();
            return Ok(value);
        }

        // PUT: api/Ciudad/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

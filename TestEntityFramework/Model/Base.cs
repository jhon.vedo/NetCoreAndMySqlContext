﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public interface IBase
    {
        DateTime ModificatedAt { get; set; }
        string ModificatedBy { get; set; }
    }
    public class Base : IBase
    {
        public DateTime ModificatedAt { get; set; }
        public string ModificatedBy { get; set; }
    }
}

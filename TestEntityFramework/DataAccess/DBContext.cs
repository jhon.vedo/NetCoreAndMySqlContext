﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Model;

namespace DataAccess
{
    public class DBContext: DbContext, IDataContextAsync
    {

        public Guid InstanceId { get; }

        public DBContext() : base()
        {
            InstanceId = Guid.NewGuid();
            this.OnConfiguring(new DbContextOptionsBuilder());

        }

        public DBContext(DbContextOptions<DBContext> options) : base(options)
        {
            InstanceId = Guid.NewGuid();
        }

        public virtual DbSet<Ciudad> Ciudad { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);


            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }

        public override int SaveChanges()
        {
            var changes = base.SaveChanges();
            return changes;
        }

        public async Task<int> SaveChangesAsync()
        {
            return await this.SaveChangesAsync(CancellationToken.None);
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            var changesAsync = await this.SaveChangesAsync(true, cancellationToken);
            return changesAsync;
        }
    }
}
